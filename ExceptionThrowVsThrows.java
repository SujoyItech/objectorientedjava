/**
 * Throw
 *
 * The throw keyword in Java is used to explicitly throw an exception from a method or any block of code. We can throw either
 * checked or unchecked exception. The throw keyword is mainly used to throw custom exceptions.
 *
 * throw Instance
 * Example:
 * throw new ArithmeticException("/ by zero");
 *
 * But this exception i.e, Instance must be of type Throwable or a subclass of Throwable. For example Exception is a sub-class of
 * Throwable and user defined exceptions typically extend Exception class. Unlike C++, data types such as int, char, floats or
 * non-throwable classes cannot be used as exceptions.
 *
 * The flow of execution of the program stops immediately after the throw statement is executed and the nearest enclosing
 * try block is checked to see if it has a catch statement that matches the type of exception.
 * If it finds a match, controlled is transferred to that statement otherwise next enclosing try block is checked and so on.
 * If no matching catch is found then the default exception handler will halt the program.
 *
 */

public class ExceptionThrowVsThrows {
    public static void main(String[] args) {
        try {
            fun();
        }catch (NullPointerException e){
            System.out.println("Caught in main.");
        }

        try {
            Tst.test();
        }catch (InterruptedException e){
            System.out.println("Caught interrupted exception.");
        }

        try {
            ThrowsExecp.fun();
        }catch (IllegalAccessException e){
            System.out.println("Caught illegal access exception.");
        }

    }

    static void fun(){
        try {
            throw new NullPointerException("Demo");

        }catch (NullPointerException e){
            System.out.println("Caught inside fun().");
            throw e;
        }
    }
}

/**
 * throws
 * throws is a keyword in Java which is used in the signature of method to indicate that this method might throw one of the listed type exceptions.
 * The caller to these methods has to handle the exception using a try-catch block.
 *
 * Important points to remember about throws keyword:
 *
 * throws keyword is required only for checked exception and usage of throws keyword for unchecked exception is meaningless.
 * throws keyword is required only to convince compiler and usage of throws keyword does not prevent abnormal termination of program.
 * By the help of throws keyword we can provide information to the caller of the method about the exception.
 */

class Tst{
    public static void test() throws InterruptedException{
        Thread.sleep(1000);
        System.out.println("Hello world");
    }
}

class ThrowsExecp
{
    static void fun() throws IllegalAccessException
    {
        System.out.println("Inside fun(). ");
        throw new IllegalAccessException("demo");
    }
}