/**
 * Main thread in Java
 * Java provides built-in support for multithreaded programming. A multi-threaded program contains two or more parts that can run concurrently.
 * Each part of such a program is called a thread, and each thread defines a separate path of execution.
 * When a Java program starts up, one thread begins running immediately. This is usually called the main thread of our program
 * because it is the one that is executed when our program begins.
 * There are certain properties associated with the main thread which are as follows:
 *
 * It is the thread from which other “child” threads will be spawned.
 * Often, it must be the last thread to finish execution because it performs various shutdown actions
 *
 * How to control Main thread
 *
 * The main thread is created automatically when our program is started. To control it we must obtain a reference to it.
 * This can be done by calling the method currentThread( ) which is present in Thread class. This method returns a reference to the thread on which it is called.
 * The default priority of Main thread is 5 and for all remaining user threads priority will be inherited from parent to child.
 *
 */
public class MultiThreadMainThread {
    public static void main(String[] args) {
        Thread t = Thread.currentThread();
        System.out.println("Current thread name => "+t.getName());
        System.out.println("Current thread stage => "+t.getState());

        t.setName("Geeks");
        System.out.println("Current thread name => "+t.getName());

        // Getting priority of Main thread
        System.out.println("Main thread priority: " + t.getPriority());
        t.setPriority(Thread.MAX_PRIORITY);
        System.out.println("Main thread priority: " + t.getPriority());

        for (int i = 0; i < 5; i++) {
            System.out.println("Main thread");
        }

        Thread ct = new Thread(){
            public void run(){
                for (int i = 0; i < 5; i++) {
                    System.out.println("Child thread");
                }
            }
        };

        System.out.println("Child thread priority: " + ct.getPriority());
        ct.setPriority(Thread.MIN_PRIORITY);
        System.out.println("Child thread priority: " + ct.getPriority());
        ct.start();

//        deadlockCreate();

    }

    /**
     * Output explanation:
     * The statement “Thread.currentThread().join()”, will tell Main thread to wait for this thread(i.e. wait for itself) to die.
     * Thus Main thread wait for itself to die, which is nothing but a deadlock.
     */
    public static void deadlockCreate(){
        try {
            System.out.println("Entering into deadlock");
            Thread.currentThread().join();
            System.out.println("This will never executed");
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}

/**
 * Now let us discuss the relationship between the main() method and the main thread in Java. For each program,
 * a Main thread is created by JVM(Java Virtual Machine). The “Main” thread first verifies the existence of the main() method,
 * and then it initializes the class. Note that from JDK 6, main() method is mandatory in a standalone java application.
 *
 * Deadlocking with use of Main Thread(only single thread)
 *
 * We can create a deadlock by just using the Main thread, i.e. by just using a single thread.
 */



