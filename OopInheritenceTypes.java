/**
 * Types of Inheritance in Java
 */

public class OopInheritenceTypes {
    public static void main(String[] args) {
        //Single Inheritence
        System.out.println("Print Single Inheritence");
        Two g = new Two();
        g.print_geek();
        g.print_for();
        g.print_geek();

        // Multi level
        System.out.println("Print Multilevel Inheritence");
        Three three = new Three();
        three.print_geek();
        three.print_for();
        three.print_geek_three();

        // Heirarical Inheritence
        System.out.println("Print Heirarical Inheritence");
        B obj_B = new B();
        obj_B.print_A();
        obj_B.print_B();

        C obj_C = new C();
        obj_C.print_A();
        obj_C.print_C();

        D obj_D = new D();
        obj_D.print_A();
        obj_D.print_D();

        // Multiple Inheritence (Interface)
        System.out.println("Print Multiple Inheritence");
        Child child = new Child();
        child.print_geek();
        child.print_for();
        child.print_geek();
    }
}

/**
 * Single Inheritance: In single inheritance, subclasses inherit the features of one superclass. In the image below, class A serves as a base class for the derived class B.
 */

class One{
    public void print_geek()
    {
        System.out.println("Geeks");
    }
}

class Two extends One{
    public void print_for() {
        System.out.println("for");
    }
}

/**
 * 2. Multilevel Inheritance: In Multilevel Inheritance, a derived class will be inheriting a base class and as well as the derived class also act as the base class to other class.
 * class A serves as a base class for the derived class B, which in turn serves as a base class for the derived class C.
 * In Java, a class cannot directly access the grandparent’s members.
 */

class Three extends Two{
    public void print_geek_three()
    {
        System.out.println("Geeks");
    }
}

/**
 3. Hierarchical Inheritance: In Hierarchical Inheritance, one class serves as a superclass (base class) for more than one subclass.
    class A serves as a base class for the derived class B, C and D.
 */

class A {
    public void print_A() { System.out.println("Class A"); }
}

class B extends A {
    public void print_B() { System.out.println("Class B"); }
}

class C extends A {
    public void print_C() { System.out.println("Class C"); }
}

class D extends A {
    public void print_D() { System.out.println("Class D"); }
}

/**

 4. Multiple Inheritance (Through Interfaces): In Multiple inheritances, one class can have more than one superclass and inherit features from all parent classes.
 Please note that Java does not support multiple inheritances with classes.
 In java, we can achieve multiple inheritances only through Interfaces. In the image below, Class C is derived from interface A and B.

 */

interface interface_one{
    public void print_geek();
}
interface interface_two{
    public void print_for();
}

interface interface_three extends interface_one,interface_two{
    public void print_geek();
}

class Child implements interface_three{
    public void print_geek(){
        System.out.println("Geek");
    }

    @Override
    public void print_for() {
        System.out.println("For");
    }
}