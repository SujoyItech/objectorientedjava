/**
 * Strings in Java are Objects that are backed internally by a char array. Since arrays are immutable(cannot grow), Strings are immutable as well. Whenever a change to a String is made, an entirely new String is created.
 *
 *  Memory allotment of String
 * Whenever a String Object is created as a literal, the object will be created in String constant pool. This allows JVM to optimize the initialization of String literal.
 *
 * CharBuffer: This class implements the CharSequence interface. This class is used to allow character buffers to be used in place of CharSequences. An example of such usage is the regular-expression package java.util.regex.
 *
 * String: String is a sequence of characters. In java, objects of String are immutable which means a constant and cannot be changed once created.
 *
 * StringBuffer:
 * StringBuffer is a peer class of String that provides much of the functionality of strings.
 * The string represents fixed-length, immutable character sequences while StringBuffer represents growable and writable character sequences.
 *
 * The StringBuilder in Java represents a mutable sequence of characters.
 * Since the String Class in Java creates an immutable sequence of characters, the StringBuilder class provides an alternate to String Class, as it creates a mutable sequence of characters.
 *
 * StringTokenizer:
 * StringTokenizer class in Java is used to break a string into tokens.
 *
 * A StringTokenizer object internally maintains a current position within the string to be tokenized. Some operations advance this current position past the characters processed.
 * A token is returned by taking a substring of the string that was used to create the StringTokenizer object.
 *
 * StringJoiner:
 * StringJoiner is a class in java.util package which is used to construct a sequence of characters(strings) separated by a delimiter
 * and optionally starting with a supplied prefix and ending with a supplied suffix.
 * Though this can also be with the help of StringBuilder class to append delimiter after each string,
 * StringJoiner provides an easy way to do that without much code to write.
 *
 */

public class StringInJava {

    public static void main(String[] args) {
        String s = "GeekforGeeks";
        System.out.println(s);
        String s1 = new String("Geekforgeeks");
        System.out.println(s1);
        System.out.println("String length = "+ s.length());
        System.out.println("Character at 3rd position = " + s.charAt(3));
        System.out.println("Substring = " + s.substring(3));
        System.out.println("Substring = " + s.substring(2,5));
        String s2 = "Geeks";
        String s3 = "forGeeks";
        System.out.println("Concatenated string  = " + s2.concat(s3));
        String s4 = "Learn Share Learn";
        System.out.println("Index of Share " + s4.indexOf("Share"));
        // Returns the index within the string of the
        // first occurrence of the specified string,
        // starting at the specified index.
        System.out.println("Index of a  = " + s4.indexOf('a',3));
        // Checking equality of Strings
        Boolean out = "Geeks".equals("geeks");
        System.out.println("Checking Equality  " + out);
        out = "Geeks".equals("Geeks");
        System.out.println("Checking Equality  " + out);


        //If ASCII difference is zero then the two strings are similar
        int out1 = s1.compareTo(s2);
        System.out.println("the difference between ASCII value is="+out1);
        // Converting cases
        String word1 = "GeeKyMe";
        System.out.println("Changing to lower Case " + word1.toLowerCase());

        // Converting cases
        String word2 = "GeekyME";
        System.out.println("Changing to UPPER Case " + word2.toUpperCase());

        // Trimming the word
        String word4 = " Learn Share Learn ";
        System.out.println("Trim the word " + word4.trim());

        // Replacing characters
        String str1 = "feeksforfeeks";
        System.out.println("Original String " + str1);
        String str2 = "feeksforfeeks".replace('f' ,'g') ;
        System.out.println("Replaced f with g -> " + str2);
    }
}
