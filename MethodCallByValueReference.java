/**
 * Pass By Value: Changes made to formal parameter do not get transmitted back to the caller.
 * Any modifications to the formal parameter variable inside the called function or method affect only the separate storage location and
 * will not be reflected in the actual parameter in the calling environment. This method is also called as call by value.
 * Java in fact is strictly call by value.
 *
 * Call by reference(aliasing): Changes made to formal parameter do get transmitted back to the caller through parameter passing.
 * Any changes to the formal parameter are reflected in the actual parameter in the calling environment as formal parameter receives a reference (or pointer) to the actual data.
 * This method is also called as call by reference. This method is efficient in both time and space.
 */

public class MethodCallByValueReference {
    public static void main(String[] args) {
        int a = 20;
        int b = 30;
        CallByValue callByValue = new CallByValue();
        callByValue.example(a,b);
        System.out.println("a=> "+a+" b=> "+b);

        CallByReference callByReference = new CallByReference(a,b);

        System.out.println("a=> "+callByReference.a+" b=> "+callByReference.b);

        callByReference.changeValue(callByReference);

        System.out.println("a=> "+callByReference.a+" b=> "+callByReference.b);

//        Java is Strictly Pass by Value!

        /**
         * In Java, all primitives like int, char, etc are similar to C/C++, but all non-primitives (or objects of any class) are always references.
         * So it gets tricky when we pass object references to methods. Java creates a copy of references and pass it to method, but they still point to same memory reference.
         * Mean if set some other object to reference passed inside method, the object from calling method as well its reference will remain unaffected.
         *
         * The changes are not reflected back if we change the object itself to refer some other location or object
         *
         */

        TestMethod testMethod = new TestMethod(5);
        change(testMethod);
        System.out.println(testMethod.x);

        /**
         * Output: 5
         * Changes are reflected back if we do not assign reference to a new location or object:
         * If we do not change the reference to refer some other object (or memory location), we can make changes to the members and these changes are reflected back.
         */

        change2(testMethod);
        System.out.println(testMethod.x);

    }

   public static void change(TestMethod t){
       t = new TestMethod();
       t.x = 10;
    }

    public static void change2(TestMethod t){
        t.x = 10;
    }


}

class CallByValue{
    public void example(int x, int y){
        x++;
        y++;
    }
}

class CallByReference{
    int a , b;
    CallByReference(int a, int b){
        this.a = a;
        this.b = b;
    }

    void  changeValue(CallByReference obj){
        obj.a += 10;
        obj.b +=20;
    }
}

class TestMethod{
    int x;
    TestMethod(int i){
        x = i;
    }

    TestMethod(){
        x = 0;
    }

}


