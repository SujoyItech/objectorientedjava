import java.util.Arrays;

public class ComparingArrays {
    public static void main(String[] args) {
        /**
         * How to compare array contents?
         * A simple way is to run a loop and compare elements one by one. Java provides a direct method Arrays.equals() to compare two arrays.
         * Actually, there is a list of equals() methods in Arrays class for different primitive types (int, char, ..etc) and one for Object type (which is base of all classes in Java).
         */

        int [] arr1 = {1,2,3};
        int [] arr2 = {1,2,3};
        if (Arrays.equals(arr1,arr2)){
            System.out.println("Same");
        }else {
            System.out.println("Not the same");
        }

        /**
         * How to Deep compare array contents?
         * As seen above, the Arrays.equals() works fine and compares arrays contents. Now the questions, what if the arrays contain arrays inside them or some other references which refer to different object but have same values.
         * For example, see the following program.
         */

        int [] inarr1 = {1,2,3};
        int [] inarr2 = {1,2,3};

        Object [] ob1 = {inarr1};
        Object [] ob2 = {inarr2};

        if (Arrays.equals(ob1,ob2)){
            System.out.println("Same");
        }else {
            System.out.println("Not the same");
        }

        /**
         * Output: Not same
         * So Arrays.equals() is not able to do deep comparison. Java provides another method for this Arrays.deepEquals() which does deep comparison.
         */

        if (Arrays.deepEquals(ob1,ob2)){
            System.out.println("Same");
        }else {
            System.out.println("Not the same");
        }

        /**
         * How does Arrays.deepEquals() work?
         * It compares two objects using any custom equals() methods they may have (if they have an equals() method implemented other than Object.equals()).
         * If not, this method will then proceed to compare the objects field by field, recursively. As each field is encountered,
         * it will attempt to use the derived equals() if it exists, otherwise it will continue to recurse further.
         * This method works on a cyclic Object graph like this: A->B->C->A. It has cycle detection so ANY two objects can be compared,
         * and it will never enter into an endless loop (Source: https://code.google.com/p/deep-equals/).
         */
    }
}
