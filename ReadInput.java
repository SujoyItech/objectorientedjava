import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * In Java, there are four different ways for reading input from the user in the command line environment(console).
 *
 */

public class ReadInput {

    public static void main(String[] args) throws IOException {
       /**
        * Using Buffered Reader Class
        This is the Java classical method to take input, Introduced in JDK1.0. This method is used by wrapping the System.in (standard input stream) in an InputStreamReader which is wrapped in a BufferedReader, we can read input from the user in the command line.
        The input is buffered for efficient reading. The wrapping code is hard to remember.
        **/

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        System.out.println(name);

        /**
         * This is probably the most preferred method to take input. The main purpose of the Scanner class is to parse primitive types and strings using regular expressions, however, it is also can be used to read input from the user in the command line.
         * Convenient methods for parsing primitives (nextInt(), nextFloat(), …) from the tokenized input.
         * Regular expressions can be used to find tokens.
         * The reading methods are not synchronized
         */

        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        System.out.println(s);

        /**
            Using Console Class
            It has been becoming a preferred way for reading user’s input from the command line. In addition, it can be used for reading password-like input without echoing the characters entered by the user; the format string syntax can also be used (like System.out.printf()).
            Advantages:
            Reading password without echoing the entered characters.
            Reading methods are synchronized.
            Format string syntax can be used.
            Does not work in non-interactive environment (such as in an IDE).
         **/

       // String new_name = System.console().readLine();
       // System.out.println(new_name);

        /**
         * Using Command line argument
         * Most used user input for competitive coding. The command-line arguments are stored in the String format.
         * The parseInt method of the Integer class converts string argument into Integer. Similarly, for float and others during execution.
         * The usage of args[] comes into existence in this input form. The passing of information takes place during the program run.
         * The command line is given to args[]. These programs have to be run on cmd.
         */

        if (args.length > 0) {
            System.out.println(
                    "The command line arguments are:");

            // iterating the args array and printing
            // the command line arguments
            for (String val : args)
                System.out.println(val);
        }
        else
            System.out.println("No command line "
                    + "arguments found.");
    }

}
