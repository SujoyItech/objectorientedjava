public class Condition {
    public static void main(String[] args) {

        // Conditions
        int i = 20;

        if (i == 10)
            System.out.println("i is 10");
        else if (i == 15)
            System.out.println("i is 15");
        else if (i == 20)
            System.out.println("i is 20");
        else
            System.out.println("i is not present");

        // Switch
        int d = 9;
        switch (d)
        {
            case 0:
                System.out.println("i is zero.");
                break;
            case 1:
                System.out.println("i is one.");
                break;
            case 2:
                System.out.println("i is two.");
                break;
            default:
                System.out.println("i is greater than 2.");
        }

        // Break

        for (int index = 0; index < 10; index++)
        {
            // terminate loop when i is 5.
            if (index == 5)
                break;

            System.out.println("index: " + index);
        }
        System.out.println("Loop complete.");

        // Java program to illustrate using break with goto


        boolean t = true;

        // label first
        first:
        {
            // Illegal statement here as label second is not
            // introduced yet break second;
            second:
            {
                third:
                {
                    // Before break
                    System.out.println("Before the break statement");

                    // break will take the control out of
                    // second label
                    if (t)
                        break second;
                    System.out.println("This won't execute.");
                }
                System.out.println("This won't execute.");
            }

            // First block
            System.out.println("This is after second block.");
        }

        // Continue

        for (int index = 0; index < 10; index++)
        {
            // If the number is even
            // skip and continue
            if (index%2 == 0)
                continue;

            // If number is odd, print it
            System.out.print(index + " ");
        }

        // Java program to illustrate using return

        System.out.println("Before the return.");

        if (t)
            return;

        // Compiler will bypass every statement
        // after return
        System.out.println("This won't execute.");
    }
}
