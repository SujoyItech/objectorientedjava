/**
 * User-defined Custom Exception in Java
 * Java provides us facility to create our own exceptions which are basically derived classes of Exception. For example MyException in below code extends the Exception class.
 *
 * We pass the string to the constructor of the super class- Exception which is obtained using “getMessage()” function on the object created.
 */
public class ExceptionUserDefined {
    public static void main(String[] args) {
        try {
            throw new MyCustomException("Geekforgeeks");
        }catch (MyCustomException exception){
            System.out.println(exception.getMessage());
        }

        try
        {
            // Throw an object of user defined exception
            throw new MyCustomException2();
        }
        catch (MyCustomException2 ex)
        {
            System.out.println("Caught");
            System.out.println(ex.getMessage());
        }
    }
}

class MyCustomException extends Exception{
    MyCustomException(String s){
        super(s);
    }
}

/**
 * In the above code, constructor of MyException requires a string as its argument.
 * The string is passed to parent class Exception’s constructor using super().
 * The constructor of Exception class can also be called without a parameter and call to super is not mandatory.
 */

class MyCustomException2 extends Exception
{

}