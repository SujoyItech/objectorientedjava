import java.util.Arrays;
import java.util.*;
//import javafx.util.Pair;

/**
 * Java doesn’t support multi-value returns. We can use following solutions to return multiple values.
 */

public class MethodReturningMultipleValues {
    public static void main(String[] args) {
        TestReturn testReturn = new TestReturn();
        int [] sub_sum = testReturn.getSubAndSum(30,10);
        System.out.println(Arrays.toString(sub_sum));

        /**
         * If there are more than two returned values
         * We can encapsulate all returned types into a class and then return an object of that class.
         */
        MulDivAdd ans = getMulDivAdd(10,20);
        System.out.println(ans.mul);
        System.out.println(ans.div);
        System.out.println(ans.add);

        /**
         * Returning list of Object Class
         *  Java program to demonstrate return of
         *  multiple values from a function using
         *  list Object class.
         */
        List<Object> person = getDetails();
        System.out.println(person);
    }

    static MulDivAdd getMulDivAdd(int a, int b){
        return new MulDivAdd(a*b,(double) a/b,a+b);
    }

    static List<Object>getDetails(){
        String name = "Geeks";
        int age = 35;
        char gender = 'M';
        return Arrays.asList(name,age,gender);
    }
}



class TestReturn{
    /**
     * If all returned elements are of same type
     * We can return an array in Java. Below is a Java program to demonstrate the same.
     */
    public int [] getSubAndSum(int a, int b){
        int [] ans = new  int[2];
        ans [0] = a+b;
        ans [1] = a-b;
        return ans;
    }

    /**
     * If returned elements are of different types
     * Using Pair (If there are only two returned values)
     */

    /**
     *
     public Pair<Integer, String> getTwo(int intVal, String strVal){
     return new Pair<Integer,String>(intVal,strVal);
     }
     */


}

class MulDivAdd{
    int mul;
    double div;
    int add;
    MulDivAdd(int mal, double div, int add){
        this.mul = mal;
        this.div = div;
        this.add = add;
    }

}

