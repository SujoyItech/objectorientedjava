/**
 Constructor chaining is the process of calling one constructor from another constructor with respect to current object.

 Constructor chaining can be done in two ways:
     Within same class: It can be done using this() keyword for constructors in same class
     From base class: by using super() keyword to call constructor from the base class.
     Constructor chaining occurs through inheritance. A sub class constructor’s task is to call super class’s constructor first.
     This ensures that creation of sub class’s object starts with the initialization of the data members of the super class.
     There could be any numbers of classes in inheritance chain. Every constructor calls up the chain till class at the top is reached.

 Why do we need constructor chaining ?
    This process is used when we want to perform multiple tasks in a single constructor rather than creating a code for each task
    in a single constructor we create a separate constructor for each task and make their chain which makes the program more readable.

 Rules of constructor chaining :
     The this() expression should always be the first line of the constructor.
     There should be at-least be one constructor without the this() keyword (constructor 3 in above example).
     Constructor chaining can be achieved in any order.

 */
public class ConstructorChaining {
    // default constructor 1
    // default constructor will call another constructor
    // using this keyword from same class
    ConstructorChaining()
    {
        // calls constructor 2
        this(5);
        System.out.println("The Default constructor");
    }

    // parameterized constructor 2
    ConstructorChaining(int x)
    {
        // calls constructor 3
        this(5, 15);
        System.out.println(x);
    }

    // parameterized constructor 3
    ConstructorChaining(int x, int y)
    {
        System.out.println(x * y);
    }

    public static void main(String args[])
    {
        // invokes default constructor first
        new ConstructorChaining();
        /**
         What happens if we change the order of constructors?
         Nothing, Constructor chaining can be achieved in any order
         */
        // invokes parameterized constructor 3
        new ConstructorChaining(8, 10);

        /**
         * NOTE: In example 1, default constructor is invoked at the end, but in example 2 default constructor is invoked at first.
         * Hence, order in constructor chaining is not important.
         * Constructor Chaining to other class using super() keyword :
         */

        Derived derived = new Derived("Test");

        /**
        Note : Similar to constructor chaining in same class, super() should be the first line of the constructor as super class’s constructor are invoked before the sub class’s constructor.

        Alternative method : using Init block :
           When we want certain common resources to be executed with every constructor we can put the code in the init block.
           Init block is always executed before any constructor, whenever a constructor is used for creating a new object.
         */

        TempConstructor tempConstructor = new TempConstructor();
        TempConstructor tempConstructor2 = new TempConstructor(10);


    }
}

class Base
{
    String name;

    // constructor 1
    Base()
    {
        this("");
        System.out.println("No-argument constructor of base class");
    }

    // constructor 2
    Base(String name)
    {
        this.name = name;
        System.out.println("Calling parameterized constructor of base");
    }
}

class Derived extends Base{
    Derived(){
        System.out.println("No argument constructor of Derived");
    }

    Derived(String name){
        super(name);
        System.out.println("Calling parameterized constructor of derived");
    }
}

class TempConstructor{
    {
        System.out.println("Init block");
    }

    TempConstructor(){
        System.out.println("Default");
    }

    TempConstructor(int x){
        System.out.println(x);
    }

    // block to be executed after the first block
    // which has been defined above.
    {
        System.out.println("second init block");
    }
}
