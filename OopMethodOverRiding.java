/**
 Overriding in Java

 In any object-oriented programming language, Overriding is a feature that allows a subclass or child class to provide a specific implementation
 of a method that is already provided by one of its super-classes or parent classes.
 When a method in a subclass has the same name, same parameters or signature, and same return type(or sub-type) as a method in its super-class,
 then the method in the subclass is said to override the method in the super-class.

 Method overriding is one of the way by which java achieve Run Time Polymorphism.
 The version of a method that is executed will be determined by the object that is used to invoke it.
 If an object of a parent class is used to invoke the method, then the version in the parent class will be executed,
 but if an object of the subclass is used to invoke the method, then the version in the child class will be executed.
 In other words, it is the type of the object being referred to (not the type of the reference variable) that determines
 which version of an overridden method will be executed.



 */
public class OopMethodOverRiding {
    public static void main(String[] args) {
        ParentClass obj1 = new ParentClass();
        obj1.show();

        ParentClass obj2 = new ChildClass();
        obj2.show();

        System.out.println("Overring and access modifier");

        ChildClass2 childClass2 = new ChildClass2();
        childClass2.m1();
        childClass2.m2();

        System.out.println("Invoking overridden methods");

        ChildClass3 childClass3 = new ChildClass3();
        childClass3.show();

    }
}

// A Simple Java program to demonstrate
// method overriding in java

class ParentClass{
    void show(){
        System.out.println("Parent method");
    }
}

class ChildClass extends ParentClass{
    void show(){
        System.out.println("Child method");
    }
}

/**
 * Rules for method overriding:
 * 1. Overriding and Access-Modifiers : The access modifier for an overriding method can allow more, but not less, access than the overridden method.
 * For example, a protected instance method in the super-class can be made public, but not private, in the subclass.
 * Doing so, will generate compile-time error.
 */

class ParentClass2{
    private void m1(){
        System.out.println("Parent class private m1");
    }
    protected void m2(){
        System.out.println("Parent class protected m2");
    }

}

class ChildClass2 extends ParentClass2{
    public void m1(){
        System.out.println("Child class public m1");
    }

    public void m2(){
        System.out.println("Child class public m2");
    }
}

/**
 2. Final methods can not be overridden : If we don’t want a method to be overridden, we declare it as final. Please see Using final with Inheritance .

 class Parent {
 // Can't be overridden
 final void show() {}
 }

 class Child extends Parent {
 // This would produce error
 void show() {}
 }

 3. Static methods can not be overridden(Method Overriding vs Method Hiding) :
 When you define a static method with same signature as a static method in base class, it is known as method hiding.
 The following table summarizes what happens when you define a method with the same signature as a method in a super-class.

 4. Private methods can not be overridden :
    Private methods cannot be overridden as they are bonded during compile time. Therefore we can’t even override private methods in a subclass.(See this for details).

 5. The overriding method must have same return type (or subtype) :
 From Java 5.0 onwards it is possible to have different return type for a overriding method in child class,
 but child’s return type should be sub-type of parent’s return type. This phenomena is known as covariant return type.

 6. Invoking overridden method from sub-class : We can call parent class method in overriding method using super keyword.

 7. Overriding and constructor :
 We can not override constructor as parent and child class can never have constructor with same name(Constructor name must always be same as Class name).



 */

class ParentClass3{
    public void show(){
        System.out.println("Parent class method");
    }
}

class ChildClass3 extends ParentClass3{
    public void show(){
        super.show();
        System.out.println("Child class method");
    }
}

/**
 Overriding and Exception-Handling : Below are two rules to note when overriding methods related to exception-handling.
 Rule#1 : If the super-class overridden method does not throw an exception, subclass overriding method can only throws the unchecked exception,
 throwing checked exception will lead to compile-time error.
 */

class ParentClass4{
    void m1()
    {
        System.out.println("From parent m1()");
    }

    void m2()
    {
        System.out.println("From parent  m2()");
    }
}

class ChildClass4 extends ParentClass4{
    // No error for unchecked exception
    void m1() throws ArithmeticException{
        System.out.println("From child m1()");
    }

    /*
      Compilation error for checked exception
        void m2() throws Exception{
            System.out.println("From child m2()");
        }
     */
}

/**
 Rule#2 : If the super-class overridden method does throws an exception, subclass overriding method can only throw same, subclass exception.
 Throwing parent exception in Exception hierarchy will lead to compile time error.
 Also there is no issue if subclass overridden method is not throwing any exception.
 */

class ParentClass5{
    void m1() throws RuntimeException{
        System.out.println("From parent m1()");
    }
}

class ChildClass5 extends ParentClass5{
    void m1() throws RuntimeException{
        System.out.println("From child5 m1()");
    }
}

class ChildClass6 extends ParentClass5{
    void m1() throws ArithmeticException{
        System.out.println("From Child6 m1()");
    }
}

class ChildClass7 extends ParentClass5{
    void m1(){
        System.out.println("From Child7 m1()");
    }
}

/**
 * This will show error
 *
 * class ChildClass8 extends ParentClass5{
 *     void m1() throws Exception{
 *         System.out.println("From child8 m1()");
 *     }
 * }
 *
 * 9. Overriding and abstract method: Abstract methods in an interface or abstract class are meant to be overridden in derived concrete
 * classes otherwise a compile-time error will be thrown.
 *
 * 10. Overriding and synchronized/strictfp method :
 * The presence of synchronized/strictfp modifier with method have no effect on the rules of overriding,
 * i.e. it’s possible that a synchronized/strictfp method can override a non synchronized/strictfp one and vice-versa.
 *
 * Overriding vs Overloading :
 *
 * 1. Overloading is about same method have different signatures. Overriding is about same method,
 * same signature but different classes connected through inheritance.
 * 2. Overloading is an example of compiler-time polymorphism and overriding is an example of run time polymorphism.
 *
 * As stated earlier, overridden methods allow Java to support run-time polymorphism. Polymorphism is essential to object-oriented
 * programming for one reason:it allows a general class to specify methods that will be common to all of its derivatives while
 * allowing subclasses to define the specific implementation of some or all of those methods. Overridden methods are another way
 * that Java implements the “one interface, multiple methods” aspect of polymorphism.
 *
 * Dynamic Method Dispatch is one of the most powerful mechanisms that object-oriented design brings to bear on code reuse and robustness.
 * The ability to exist code libraries to call methods on instances of new classes without recompiling while maintaining a clean abstract
 * interface is a profoundly powerful tool.
 *
 * Overridden methods allow us to call methods of any of the derived classes without even knowing the type of derived class object.
 *
 *
 *
 */
