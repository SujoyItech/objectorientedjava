public class StringBuilderExample {
    public static void main(String[] args) throws Exception {
        StringBuilder str = new StringBuilder();
        str.append("Geek");
        System.out.println(str.toString());

        StringBuilder str2 = new StringBuilder(10);
        System.out.println(str2.capacity());

        StringBuilder reverseString = str.reverse();
        System.out.println(reverseString);

        str.appendCodePoint(44);
        System.out.println(str);
    }
}
