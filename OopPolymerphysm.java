/**

 Polymorphism in Java

 The word polymorphism means having many forms. In simple words, we can define polymorphism as the ability of a message to be displayed in more than one form.

 Real life example of polymorphism: A person at the same time can have different characteristic.
 Like a man at the same time is a father, a husband, an employee. So the same person posses different behavior in different situations.
 This is called polymorphism.

 Polymorphism is considered one of the important features of Object-Oriented Programming.
 Polymorphism allows us to perform a single action in different ways.
 In other words, polymorphism allows you to define one interface and have multiple implementations.
 The word “poly” means many and “morphs” means forms, So it means many forms.

 In Java polymorphism is mainly divided into two types:
 Compile time Polymorphism
 Runtime Polymorphism

 1. Compile-time polymorphism: It is also known as static polymorphism.
 This type of polymorphism is achieved by function overloading or operator overloading. But Java doesn’t support the Operator Overloading.

 2. Runtime polymorphism: It is also known as Dynamic Method Dispatch.
 It is a process in which a function call to the overridden method is resolved at Runtime.
 This type of polymorphism is achieved by Method Overriding.

 */

public class OopPolymerphysm {
    public static void main(String[] args) {
        System.out.println(MultiplyFunction.multiply(5,10));
        System.out.println(MultiplyFunction.multiply(5.5,10.5));
        System.out.println(MultiplyFunction.multiply(5,10, 20));

        Parent a;
        a = new Subclass1();
        a.print();
        a = new Subclass2();
        a.print();
    }
}

/**
 * Method Overloading: When there are multiple functions with same name but different parameters then these functions are said to be overloaded.
 * Functions can be overloaded by change in number of arguments or/and change in type of arguments.
 */
class MultiplyFunction{
    static int multiply(int a, int b){
        return a*b;
    }

    static double multiply(double a, double b){
        return a*b;
    }

//    Example 2: By using different numbers of arguments

    static int multiply(int a, int b, int c){
        return a * b * c;
    }
}

/**
 Method overriding, on the other hand, occurs when a derived class has a definition for one of the member functions of the base class.
 That base function is said to be overridden.
 */

class Parent{
    void print(){
        System.out.println("Parent class");
    }
}

class Subclass1 extends Parent{
    void print(){
        System.out.println("Sub class 1");
    }
}

class Subclass2 extends Parent{
    void print(){
        System.out.println("Sub class 2");
    }
}
