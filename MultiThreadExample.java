/**
 * Multithreading in Java
 * Multithreading is a Java feature that allows concurrent execution of two or more parts of a program for maximum utilization of CPU.
 * Each part of such program is called a thread. So, threads are light-weight processes within a process.
 *
 * Threads can be created by using two mechanisms :
 *
 * Extending the Thread class
 * Implementing the Runnable Interface
 *
 */

public class MultiThreadExample {
    public static void main(String[] args) {
        for (int i=0; i<8;i++){
            MultiThreadingDemo object = new MultiThreadingDemo();
            object.start();
        }

        System.out.println("First method completed");

        for (int i=0; i<8;i++){
            Thread object2 = new Thread(new MultiThreadingDemo2());
            object2.start();
        }
    }
}

/**
 * Thread creation by extending the Thread class
 * We create a class that extends the java.lang.Thread class. This class overrides the run() method available in the Thread class.
 * A thread begins its life inside run() method.
 * We create an object of our new class and call start() method to start the execution of a thread.
 * Start() invokes the run() method on the Thread object.
 */

class MultiThreadingDemo extends Thread{
    public void run(){
        try {
            System.out.println("Thread " + Thread.currentThread().getId() + " is running");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}

/**
 * Thread creation by implementing the Runnable Interface
 * We create a new class which implements java.lang.Runnable interface and override run() method.
 * Then we instantiate a Thread object and call start() method on this object.
 */

class MultiThreadingDemo2 implements Runnable{

    @Override
    public void run() {
        try {
            // Displaying the thread that is running
            System.out.println("Thread " + Thread.currentThread().getId() + " is running");
        }
        catch (Exception e) {
            // Throwing an exception
            System.out.println("Exception is caught");
        }
    }
}

/**
 * Thread Class vs Runnable Interface
 *
 * If we extend the Thread class, our class cannot extend any other class because Java doesn’t support multiple inheritance.
 * But, if we implement the Runnable interface, our class can still extend other base classes.
 * We can achieve basic functionality of a thread by extending Thread class because it provides some
 * inbuilt methods like yield(), interrupt() etc. that are not available in Runnable interface.
 * Using runnable will give you an object that can be shared amongst multiple threads.
 */