/**
 Method overriding is one of the ways in which Java supports Runtime Polymorphism. Dynamic method dispatch is the mechanism
 by which a call to an overridden method is resolved at run time, rather than compile time

 When an overridden method is called through a superclass reference, Java determines which version(superclass/subclasses)
 of that method is to be executed based upon the type of the object being referred to at the time the call occurs. Thus,
 this determination is made at run time.

 At run-time, it depends on the type of the object being referred to (not the type of the reference variable) that determines
 which version of an overridden method will be executed

 A superclass reference variable can refer to a subclass object. This is also known as upcasting. Java uses this fact to resolve
 calls to overridden methods at run time.

 Therefore, if a superclass contains a method that is overridden by a subclass, then when different types of objects are referred
 to through a superclass reference variable, different versions of the method are executed.

 Here is an example that illustrates dynamic method dispatch:

 */
public class OopDynamicMethodDispatch {
    public static void main(String[] args) {
        ClassA a = new ClassA();
        ClassB b = new ClassB();
        ClassC c = new ClassC();

        // obtain a reference of type ClassA
        ClassA ref;
        ref = a;
        ref.m1();

        ref = b;
        ref.m1();

        ref = c;
        ref.m1();
    }
}

// A Java program to illustrate Dynamic Method
// Dispatch using hierarchical inheritance
class ClassA
{
    void m1()
    {
        System.out.println("Inside A's m1 method");
    }
}

class ClassB extends ClassA
{
    // overriding m1()
    void m1()
    {
        System.out.println("Inside B's m1 method");
    }
}

class ClassC extends ClassA
{
    // overriding m1()
    void m1()
    {
        System.out.println("Inside C's m1 method");
    }
}

/**
 * Explanation :

 The above program creates one superclass called A and it’s two subclasses B and C. These subclasses overrides m1( ) method.

 1. Inside the main() method in Dispatch class, initially objects of type A, B, and C are declared
 2. Now a reference of type A, called ref, is also declared, initially it will point to null.
 3. Now we are assigning a reference to each type of object (either A’s or B’s or C’s) to ref, one-by-one, and uses that reference to invoke m1( ).
 As the output shows, the version of m1( ) executed is determined by the type of object being referred to at the time of the call.

 Runtime Polymorphism with Data Members
 In Java, we can override methods only, not the variables(data members), so runtime polymorphism cannot be achieved by data members.

 Advantages of Dynamic Method Dispatch

 1. Dynamic method dispatch allow Java to support overriding of methods which is central for run-time polymorphism.
 2. It allows a class to specify methods that will be common to all of its derivatives, while allowing subclasses to
 define the specific implementation of some or all of those methods.
 3. It also allow subclasses to add its specific methods subclasses to define the specific implementation of some.

 Static vs Dynamic binding

 Static binding is done during compile-time while dynamic binding is done during run-time.

 private, final and static methods and variables uses static binding and bonded by compiler while overridden methods are bonded
 during runtime based upon type of runtime object

 */