import java.util.Arrays;

/**
 * Arrays.toString() method: Arrays.toString() method is used to return a string representation of the contents of the specified array.
 * The string representation consists of a list of the array’s elements, enclosed in square brackets (“[]”).
 * Adjacent elements are separated by the characters “, ” (a comma followed by a space). It returns “null” if the array is null.
 */

public class ArrayToString {
    public static void main(String[] args) {
        boolean bool_arr [] = new boolean[] {true,false,true,true};
        char char_arr [] = new char[] {'a','b','c','d'};
        double double_arr [] = new double[] {1,2,3,4};
        int int_arr [] = new  int[] {1,2,3,4};
        Object [] object_arr = new Object[]{1,2,3};

        System.out.println("Boolean Array "+Arrays.toString(bool_arr));
        System.out.println("Char Array "+Arrays.toString(char_arr));
        System.out.println("Double Array "+Arrays.toString(double_arr));
        System.out.println("Int Array "+Arrays.toString(int_arr));
        System.out.println("Object Array "+Arrays.toString(object_arr));

        /**
         * StringBuilder append(char[]): The java.lang.StringBuilder.append(char[]) is the inbuilt method which appends the string representation of the char array argument to this StringBuilder sequence.
         */

        StringBuffer stringBuffer = new StringBuffer("We are geeks");
        System.out.println(stringBuffer);
        char [] astr = new char[] {'G', 'E', 'E', 'K', 'S'};
        stringBuffer.append(astr);
        System.out.println(stringBuffer);

    }
}
