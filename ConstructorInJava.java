/**
 * Constructors are used to initialize the object’s state. Like methods, a constructor also contains a collection of statements(i.e. instructions) that are executed at the time of Object creation.
 *
 * Constructors are used to assign values to the class variables at the time of object creation, either explicitly done by the programmer or by Java itself (default constructor).
 *
 * When is a Constructor called?
 * Each time an object is created using a new() keyword, at least one constructor (it could be the default constructor) is invoked to assign initial values to the data members of the same class.
 * A constructor is invoked at the time of object or instance creation. For Example:
 *
 * Rules for writing Constructor:
 * Constructor(s) of a class must have the same name as the class name in which it resides.
 * A constructor in Java can not be abstract, final, static and Synchronized.
 * Access modifiers can be used in constructor declaration to control its access i.e which other class can call the constructor.
 *
 * Types of constructor
 * There are two types of constructor in Java:
 *
 * 1. No-argument constructor: A constructor that has no parameter is known as the default constructor. If we don’t define a constructor in a class,
 * then the compiler creates default constructor(with no arguments) for the class.
 * And if we write a constructor with arguments or no-arguments then the compiler does not create a default constructor.
 * Default constructor provides the default values to the object like 0, null, etc. depending on the type.
 *
 * 2. Parameterized Constructor: A constructor that has parameters is known as parameterized constructor.
 * If we want to initialize fields of the class with your own values, then use a parameterized constructor.
 *
 * Does constructor return any value?
 * There are no “return value” statements in the constructor, but the constructor returns the current class instance. We can write ‘return’ inside a constructor.
 *
 */
public class ConstructorInJava {
    public static void main(String[] args) {
        // this would invoke default constructor.
        Geek geek1 = new Geek();

        // Default constructor provides the default
        // values to the object like 0, null
        System.out.println(geek1.name);
        System.out.println(geek1.num);

        // this would invoke the parameterized constructor.
        Geek2 geek2 = new Geek2("adam", 1);
        System.out.println("GeekName :" + geek2.name +
                " and GeekId :" + geek2.id);

        /**
         * Constructor Overloading
         * Like methods, we can overload constructors for creating objects in different ways.
         * Compiler differentiates constructors on the basis of numbers of parameters, types of the parameters and order of the parameters.
         */
        // Creating the objects of the class named 'Geek'
        // by passing different arguments

        // Invoke the constructor with one argument of
        // type 'String'.
        GeekForGeeks geekForGeeks1 = new GeekForGeeks("Shikhar");

        // Invoke the constructor with two arguments
        GeekForGeeks geekForGeeks2 = new GeekForGeeks("Dharmesh", 26);

        // Invoke the constructor with one argument of
        // type 'Long'.
        GeekForGeeks geekForGeeks3 = new GeekForGeeks(325614567);
    }
}

class Geek{
    int num = 10;
    String name = "Sujoy";

    // this would be invoked while an object
    // of that class is created.
    Geek()
    {
        System.out.println("Constructor called");
    }
}
class Geek2{
    // data members of the class.
    String name;
    int id;

    // constructor would initialize data members
    // with the values of passed arguments while
    // object of that class created.
    Geek2(String name, int id)
    {
        this.name = name;
        this.id = id;
    }
}

class GeekForGeeks{
    GeekForGeeks(String name){
        System.out.println("Constructor with one " + "argument - String : " + name);
    }
    GeekForGeeks(String name, int age){
        System.out.println("Constructor with two arguments : " + " String and Integer : " + name + " "+ age);
    }
    GeekForGeeks(long id){
        System.out.println("Constructor with one argument : " + "Long : " + id);
    }
}


