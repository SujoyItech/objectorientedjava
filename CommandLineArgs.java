public class CommandLineArgs {
    public static void main(String[] args) {
        if (args.length > 0){
            System.out.println("Command Line arguments are");
            for (String val:args){
                System.out.println(val);
            }
        }else {
            System.out.println("No common line argument found");
        }
    }
}
