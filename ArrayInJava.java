public class ArrayInJava {
    public static void main(String[] args) {
        int [] arr;
        arr = new int[5];
        arr[0] = 10;
        arr[1] = 20;
        arr[2] = 30;
        arr[3] = 40;

        for (int i = 0; i < arr.length-1 ;i++){
            System.out.println(arr[i]);
        }

        Student [] student;

        student = new Student[5];
        student[0] = new Student(1, "Sujoy");
        student[1] = new Student(2, "Sujon");
        student[2] = new Student(3, "Sumon");
        student[3] = new Student(1, "Sandip");

        for (int i=0; i<student.length-1; i++){
            System.out.println(student[i].roll_no +" => "+ student[i].name);
        }

        int arr2D[][] = { {2,7,9},{3,6,1},{7,4,2} };

        for (int i=0;i<3;i++){
            for (int j=0; j<3; j++){
                System.out.print(arr2D[i][j] + " ");
            }
            System.out.println("");
        }

        int newInt[] = m1();

        for (int i=0; i<newInt.length; i++){
            System.out.println(newInt[i]);
        }

        int intArray [] = {1,2,4};
        int cloneArray [] = intArray.clone();

        int intArray2D[][]  = {{1,2,3},{2,4,6},{7,8,9}};
        int intArrayClone2D[][]  = intArray2D.clone();

        System.out.println("Contents of 2D Jagged Array");

        int newArray [][] = new int[2][];
        newArray[0] = new int[3];
        newArray[1] = new int[2];
        int count = 0;
        for (int i=0; i<newArray.length; i++){
            for (int j=0; j<newArray[i].length;j++){
                newArray[i][j] = count++;
            }
        }

        for (int i=0; i<newArray.length ; i++){
            for (int j=0; j<newArray[i].length; j++){
                System.out.print(newArray[i][j]+" ");
            }
            System.out.println();
        }

        int newArr2 [][] = new int[5][];

        for (int i =0 ; i< newArr2.length ; i++){
            newArr2[i] = new int[i+1];
        }

        count = 0;

        for (int i =0; i<newArr2.length;i++){
            for (int j=0; j<newArr2[i].length; j++){
                newArr2[i][j] = count ++;
            }
        }

        System.out.println("Print 2D Jagged Array modified");

        for (int i=0; i<newArr2.length;i++){
            for (int j=0; j<newArr2[i].length; j++){
                System.out.print(newArr2[i][j]+" ");
            }
            System.out.println();
        }


    }

    public static int [] m1(){
        return new int[]{1,2,3};
    }
}

class Student{
    public int roll_no;
    public String name;
    Student(int roll_no,String name){
        this.roll_no = roll_no;
        this.name = name;
    }
}