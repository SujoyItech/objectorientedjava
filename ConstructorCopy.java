public class ConstructorCopy {
    public static void main(String[] args) {
        Complex c1 = new Complex(10,15);
        Complex c2 = new Complex(c1);
        Complex c3 = c2;
        System.out.println(c2);
    }
}

class Complex{
    private double re,rm;
    // A normal parameterized constructor
    public Complex(double re,double rm){
        this.re = re;
        this.rm = rm;
    }
// copy constructor
    Complex(Complex c){
        System.out.println("Copy constructor called");
        re = c.re;
        rm = c.rm;
    }

    // Overriding the toString of Object class
    @Override
    public String toString() {
        return "(" + re + " + " + rm + "i)";
    }
}