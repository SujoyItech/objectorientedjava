/**
 * Synchronized in Java
 *
 * Multi-threaded programs may often come to a situation where multiple threads try to access the same resources
 * and finally produce erroneous and unforeseen results.
 * So it needs to be made sure by some synchronization method that only one thread can access the resource at a given point of time.
 * Java provides a way of creating threads and synchronizing their task by using synchronized blocks. Synchronized blocks in Java are marked
 * with the synchronized keyword. A synchronized block in Java is synchronized on some object. All synchronized blocks synchronized
 * on the same object can only have one thread executing inside them at a time. All other threads attempting to enter the synchronized block
 * are blocked until the thread inside the synchronized block exits the block.
 *
 * This synchronization is implemented in Java with a concept called monitors. Only one thread can own a monitor at a given time.
 * When a thread acquires a lock, it is said to have entered the monitor. All other threads attempting to enter the locked monitor
 * will be suspended until the first thread exits the monitor.
 *
 */
public class MultiThreadSynchronization {

    public static void main(String[] args) {
        Sender sender = new Sender();
        ThreadSend s1 = new ThreadSend("Hello world", sender);
        ThreadSend s2 = new ThreadSend("Hi how are you?", sender);
        s1.start();
        s2.start();
        try {
            s1.join();
            s2.join();
        }catch (InterruptedException e){

        }
    }

}

class Sender{
    public void send(String msg){
        System.out.println("Sending\t"  + msg );
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}

class ThreadSend extends Thread{
    private String msg;
    Sender sender;
    ThreadSend(String msg, Sender sender){
        this.msg = msg;
        this.sender = sender;
    }

    public void run(){
        synchronized (sender){
            sender.send(msg);
        }
    }
}