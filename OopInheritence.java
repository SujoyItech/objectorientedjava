/**
 Inheritance in Java

 Inheritance is an important pillar of OOP(Object-Oriented Programming).
 It is the mechanism in java by which one class is allowed to inherit the features(fields and methods) of another class.

 Important terminology:

 Super Class: The class whose features are inherited is known as superclass(or a base class or a parent class).

 Sub Class: The class that inherits the other class is known as a subclass(or a derived class, extended class, or child class).
 The subclass can add its own fields and methods in addition to the superclass fields and methods.

 Reusability: Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and
 there is already a class that includes some of the code that we want, we can derive our new class from the existing class.
 By doing this, we are reusing the fields and methods of the existing class.


 */
public class OopInheritence {
    public static void main(String[] args) {
        MountainBike mb = new MountainBike(3, 100, 25);
        System.out.println(mb.toString());
        mb.applyBreak(10);
        System.out.println(mb.toString());
    }
}

class Bicycle{
    public int gear;
    public int speed;
    public Bicycle(int gear, int speed){
        this.gear = gear;
        this.speed = speed;
    }

    public void applyBreak(int decrement){
        speed -= decrement;
    }

    public void speedUp(int increment){
        speed += increment;
    }

    public String toString(){
        return ("No of gears are " + gear + "\n" + "speed of bicycle is " + speed);
    }
}

class MountainBike extends Bicycle{
    public int seatHeight;
    public MountainBike(int gear, int speed, int seatHeight){
        super(gear,speed);
        this.seatHeight = seatHeight;
    }

    public void setSeatHeight(int newValue){
        seatHeight += newValue;
    }

    public String toString(){
        return super.toString()+"\nSeat height is "+seatHeight;
    }
}