/**
 * java.lang.Thread class provides the join() method which allows one thread to wait until another thread completes its execution.
 * If t is a Thread object whose thread is currently executing, then t.join() will make sure that t is terminated before the next instruction
 * is executed by the program.
 * If there are multiple threads calling the join() methods that means overloading on join allows the programmer to specify a waiting period.
 * However, as with sleep, join is dependent on the OS for timing, so you should not assume that join will wait exactly as long as you specify.
 */
public class MultiThreadJoin {
    public static void main(String[] args) {
        ThreadJoining t1 = new ThreadJoining();
        ThreadJoining t2 = new ThreadJoining();
        ThreadJoining t3 = new ThreadJoining();
        t1.start();
        // starts second thread after when
        // first thread t1 has died.
        try
        {
            System.out.println("Current Thread: " + Thread.currentThread().getName());
            t1.join();
        }catch(Exception ex)
        {
            System.out.println("Exception has " + "been caught" + ex);
        }
        t2.start();
        try
        {
            System.out.println("Current Thread: " + Thread.currentThread().getName());
            t2.join();
        }catch(Exception ex)
        {
            System.out.println("Exception has " + "been caught" + ex);
        }
        t3.start();
        try
        {
            System.out.println("Current Thread: " + Thread.currentThread().getName());
            t2.join();
        }catch(Exception ex)
        {
            System.out.println("Exception has " + "been caught" + ex);
        }

    }
}

class ThreadJoining extends Thread{
    public void run()
    {
        for (int i = 0; i < 2; i++)
        {
            try
            {
                Thread.sleep(500);
                System.out.println("Current Thread: " + Thread.currentThread().getName());
            }

            catch(Exception ex)
            {
                System.out.println("Exception has" + " been caught" + ex);
            }
            System.out.println(i);
        }
    }
}
