public class ConstructorOverloading {
    public static void main(String[] args) {
        Box box = new Box();
        System.out.println(box.volume());
        Box box2 = new Box(10,20,30);
        System.out.println(box2.volume());
        Box box3 = new Box(3);
        System.out.println(box3.volume());
    }
}


// Java program to illustrate
// Constructor Overloading
class Box
{
    double width, height, depth;

    // constructor used when all dimensions
    // specified
    Box(double w, double h, double d)
    {
        width = w;
        height = h;
        depth = d;
    }

    // constructor used when no dimensions
    // specified
    Box()
    {
        width = height = depth = 0;
    }

    // constructor used when cube is created
    Box(double len)
    {
        // this() is used for calling the default
        // constructor from parameterized constructor
//        this();
        width = height = depth = len;
    }

    // compute and return volume
    double volume()
    {
        return width * height * depth;
    }
}