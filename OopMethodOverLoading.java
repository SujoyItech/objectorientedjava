/**

 Overloading allows different methods to have the same name, but different signatures where the signature can differ
 by the number of input parameters or type of input parameters or both. Overloading is related to compile-time (or static) polymorphism.



 */

public class OopMethodOverLoading {
    public static void main(String[] args) {
        Sum s = new Sum();
        System.out.println(s.sum(10, 20));
        System.out.println(s.sum(10, 20, 30));
        System.out.println(s.sum(10.5, 20.5));

        byte a = 25;
        Demo obj = new Demo();
        obj.show(a); // it will go to
        // byte argument
        obj.show("hello"); // String
        obj.show(250); // Int
        obj.show('A'); // Since char is
        // not available, so the datatype
        // higher than char in terms of
        // range is int.
        obj.show("A"); // String
//        obj.show(7.5); // since float datatype
    }
}

class Sum{
    public int sum(int x, int y)
    {
        return (x + y);
    }

    // Overloaded sum(). This sum takes three int parameters
    public int sum(int x, int y, int z)
    {
        return (x + y + z);
    }

    // Overloaded sum(). This sum takes two double parameters
    public double sum(double x, double y)
    {
        return (x + y);
    }
}

/**
 Question Arises:
 Q. What if the exact prototype does not match with arguments.
 Ans.
 Priority wise, compiler take these steps:

 Type Conversion but to higher type(in terms of range) in same family.
 Type conversion to next higher family(suppose if there is no long data type available for an int data type, then it will search for the float data type).
 */

class Demo {
    public void show(int x) {
        System.out.println("In int" + x);
    }

    public void show(String s) {
        System.out.println("In String" + s);
    }

    public void show(byte b) {
        System.out.println("In byte" + b);
    }
}

/**

 What is the advantage?
 We don’t have to create and remember different names for functions doing the same thing. For example, in our code,
 if overloading was not supported by Java, we would have to create method names like sum1, sum2, … or sum2Int, sum3Int, … etc.

 Can we overload methods on return type?
 We cannot overload by return type. This behavior is same in C++. Refer this for details

 However, Overloading methods on return type are possible in cases where the data type of the function being called is explicitly specified. Look at the examples below :

 Can we overload static methods?
 The answer is ‘Yes’. We can have two ore more static methods with same name, but differences in input parameters. For example, consider the following Java program. Refer this for details.

 Can we overload methods that differ only by static keyword?
 We cannot overload two methods in Java if they differ only by static keyword (number of parameters and types of parameters is same).
 See following Java program for example. Refer this for details.

 Can we overload main() in Java?
 Like other static methods, we can overload main() in Java. Refer overloading main() in Java for more details.

 */

 class TestData {

    // Normal main()
    public static void main(String[] args)
    {
        System.out.println("Hi Geek (from main)");
        TestData.main("Geek");
    }

    // Overloaded main methods
    public static void main(String arg1)
    {
        System.out.println("Hi, " + arg1);
        TestData.main("Dear Geek", "My Geek");
    }
    public static void main(String arg1, String arg2)
    {
        System.out.println("Hi, " + arg1 + ", " + arg2);
    }
}