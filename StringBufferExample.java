/**
 * String Class in Java
 *
 * StringBuffer is a peer class of String that provides much of the functionality of strings. String represents fixed-length,
 * immutable character sequences while StringBuffer represents growable and writable character sequences.
 *
 * StringBuffer may have characters and substrings inserted in the middle or appended to the end.
 * It will automatically grow to make room for such additions and often has more characters preallocated than are actually needed, to allow room for growth.
 *
 */

public class StringBufferExample {
    public static void main(String[] args) throws Exception {
        StringBuffer s = new StringBuffer("GeeksforGeeks");
        int p = s.length();
        int q = s.capacity();
        System.out.println("Length of string GeeksforGeeks=" + p);
        System.out.println("Capacity of string GeeksforGeeks=" + q);

        s.append("Geeks");
        System.out.println(s);
        s.insert(5,"for");
        System.out.println(s);
        s.reverse();
        System.out.println(s);
        s.replace(5,8,"are");
        System.out.println(s);
    }
}
