import java.util.Arrays;

/**
 * Given an array, we need to copy its elements in a different array.
 * Method 1 (Simple but Wrong)
 * We might be tempted to proceed like this:
 */

public class ArrayCopy {
    public static void main(String[] args) {
        int a[] = { 1,2,3 };
        int b[] = new int[a.length];
        b = a;
        for (int i=0; i<b.length; i++){
            System.out.println(i);
        }

        /**
         * When we do “b = a”, we are actually assigning a reference to the array. Hence, if we make any change to one array, it would be reflected in other arrays as well because both a and b refer to the same location.
         *
         * Method 2: (Easy and correct)
         * We might iterate each element of the given original array and copy one element at a time.
         * Using this method guarantees that any modifications to b, will not alter the original array a
         */

        int new_array_a[] = {1, 8, 3};
        int new_array_b[] = new int[a.length];

        for (int i=0; i<new_array_a.length; i++){
            new_array_b[i] = new_array_a[i];
        }

        for (int i=0; i<new_array_b.length;i++){
            System.out.println(new_array_b[i]);
        }

        /**
         * Method 3: ( Using Clone() )
         *
         * In the previous method we had to iterate over the entire array to make a copy, can we do better? The answer is YES!
         * We can use the clone method in Java.
         */

        System.out.println("Array clone");

        int new_array_2_a [] = {1,8,3};
        int new_array_2_b [] = new_array_2_a.clone();
        for (int i=0; i<new_array_2_b.length;i++){
            System.out.print(new_array_2_b[i]);
        }

        System.out.println();

        /**
         * Method 4: ( Using System.arraycopy() )
         * Where:
         * src denotes the source array.
         * srcPos is the index from which copying starts.
         * dest denotes the destination array
         * destPos is the index from which the copied elements are placed in the destination array.
         * length is the length of the subarray to be copied.
         */

        System.out.println("Array copy");

        int array_original [] = {1, 2, 3};
        int array_copy [] = new int[array_original.length];
        System.arraycopy(array_original,0,array_copy,0,3);
        for (int i=0; i<array_copy.length;i++){
            System.out.print(array_copy[i]);
        }
        System.out.println();

        /**
         * Method 5: ( Using Arrays.copyOf( ))
         *If you want to copy the first few elements of an array or full copy of the array, you can use this method. Its syntax is as:
         */
        System.out.println("Array copy of");
        int arrayCopyOf_a [] = {1, 8, 3 };
        int arrayCopyOf_b [] = Arrays.copyOf(arrayCopyOf_a, 3);
        for (int i=0; i<arrayCopyOf_b.length;i++){
            System.out.print(arrayCopyOf_b[i]);
        }

        /**
         * Method 6: ( Using Arrays.copyOfRange())
         * This method copies the specified range of the specified array into a new array.
         */
        System.out.println();

        int arrayCopyRange_a [] = {1, 4 , 3};
        int arrayCopyRange_b [] = Arrays.copyOfRange(arrayCopyRange_a, 0,3);
        System.out.println("Array copy range");
        for (int i=0; i<arrayCopyRange_b.length;i++){
            System.out.print(arrayCopyRange_b[i]);
        }
        System.out.println();

        /**
         * Overview of the above methods:
         *
         * Simply assigning reference is wrong
         * The array can be copied by iterating over an array, and one by one assigning elements.
         * We can avoid iteration over elements using clone() or System.arraycopy()
         * clone() creates a new array of the same size, but System.arraycopy() can be used to copy from a source range to a destination range.
         * System.arraycopy() is faster than clone() as it uses Java Native Interface (Source : StackOverflow)
         * If you want to copy the first few elements of an array or a full copy of an array, you can use Arrays.copyOf() method.
         * Arrays.copyOfRange() is used to copy a specified range of an array. If the starting index is not 0, you can use this method to copy a partial array.
         */
    }
}
