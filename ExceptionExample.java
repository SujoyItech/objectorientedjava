import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 Types of Exception in Java with Examples

 Two Types of Exception: Build in Exception, User Defined Exception

 Built-in Exceptions
 Built-in exceptions are the exceptions which are available in Java libraries. These exceptions are suitable to explain certain error situations.
 Below is the list of important built-in exceptions in Java.

 1. ArithmeticException
    It is thrown when an exceptional condition has occurred in an arithmetic operation.
 2. ArrayIndexOutOfBoundsException
    It is thrown to indicate that an array has been accessed with an illegal index. The index is either negative or greater than or equal to the size of the array.
 3. ClassNotFoundException
    This Exception is raised when we try to access a class whose definition is not found
 4. FileNotFoundException
    This Exception is raised when a file is not accessible or does not open.
 5. IOException
    It is thrown when an input-output operation failed or interrupted
 6. InterruptedException
    It is thrown when a thread is waiting, sleeping, or doing some processing, and it is interrupted.
 7. NoSuchFieldException
    It is thrown when a class does not contain the field (or variable) specified
 8. NoSuchMethodException
    It is thrown when accessing a method which is not found.
 9. NullPointerException
    This exception is raised when referring to the members of a null object. Null represents nothing
 10. NumberFormatException
    This exception is raised when a method could not convert a string into a numeric format.
 11. RuntimeException
    This represents any exception which occurs during runtime.
 12. StringIndexOutOfBoundsException
    It is thrown by String class methods to indicate that an index is either negative or greater than the size of the string

 */

public class ExceptionExample {
    public static void main(String[] args) {
        System.out.println("Arithmetic Exception demo");
        arithmeticExceptionDemo();
        System.out.println("Null pointer exception demo");
        nullPointerExceptionDemo();
        System.out.println("String index out of bound exception demo");
        stringOutOfBoundException();
        System.out.println("File not found exception demo");
        fileNotFoundException();
        System.out.println("Number not found exception demo");
        numberNotFoundException();
        System.out.println("Array index out of bound exception demo");
        arrayIndexOutOfBoundException();

        System.out.println("User defined function");
        MyException.myException();
    }

    static void arithmeticExceptionDemo(){
        try {
            int a = 30, b = 0;
            int c = a/b;  // cannot divide by zero
            System.out.println ("Result = " + c);
        }catch (ArithmeticException exception){
            System.out.println ("Can't divide a number by 0");
        }
    }

    static void nullPointerExceptionDemo(){
        try {
            String a = null; //null value
            System.out.println(a.charAt(0));
        }catch (NullPointerException exception){
            System.out.println("NullPointerException..");
        }
    }
    static void stringOutOfBoundException(){
        try {
            String a = "This is like chipping "; // length is 22
            char c = a.charAt(24); // accessing 25th element
            System.out.println(c);
        }catch (StringIndexOutOfBoundsException exception){
            System.out.println("String index out of bound exception..");
        }
    }
    static void fileNotFoundException(){
        try {
            // Following file does not exist
            FileReader fr = new FileReader(new File("E://file.txt"));
        }catch (FileNotFoundException exception){
            System.out.println("File not found exception");
        }
    }
    static void numberNotFoundException(){
        try {
            // "akki" is not a number
            int num = Integer.parseInt ("akki") ;

            System.out.println(num);
        } catch(NumberFormatException e) {
            System.out.println("Number format exception");
        }
    }

    static void arrayIndexOutOfBoundException(){
        try{
            int a[] = new int[5];
            a[6] = 9; // accessing 7th element in an array of
            // size 5
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println ("Array Index is Out Of Bounds");
        }
    }

}

/**
 * User-Defined Exceptions
 * Sometimes, the built-in exceptions in Java are not able to describe a certain situation. In such cases, user can also
 * create exceptions which are called ‘user-defined Exceptions’.
 *
 * Following steps are followed for the creation of user-defined Exception.
 *
 * The user should create an exception class as a subclass of Exception class. Since all the exceptions are subclasses of Exception class,
 * the user should also make his class a subclass of it. This is done as:
 * class MyException extends Exception
 *
 * We can write a default constructor in his own exception class.
 * MyException(){}
 *
 * We can also create a parameterized constructor with a string as a parameter.
 * We can use this to store exception details. We can call super class(Exception) constructor from this and send the string there.
 *
 * MyException(String str)
 * {
 *    super(str);
 * }
 *
 * To raise exception of user-defined type, we need to create an object to his exception class and throw it using throw clause, as:
 *
 * MyException me = new MyException(“Exception details”);
 * throw me;
 *
 * The following program illustrates how to create own exception class MyException.
 * Details of account numbers, customer names, and balance amounts are taken in the form of three arrays.
 * In main() method, the details are displayed using a for-loop. At this time, check is done if in any account
 * the balance amount is less than the minimum balance amount to be apt in the account.
 * If it is so, then MyException is raised and a message is displayed “Balance amount is less”.
 *
 */

class MyException extends Exception{
    private static int [] accno = {1001,1002,1003,1004};
    private static String [] name = {"Nish", "Shubh", "Sush", "Abhi", "Akash"};
    private static double[] bal = {10000.00, 12000.00, 5600.0, 999.00, 1100.55};

    MyException(){

    }
    MyException(String str){
        super(str);
    }

    public static void myException(){
        try {
            // display the heading for the table
            System.out.println("ACCNO" + "\t" + "CUSTOMER" + "\t" + "BALANCE");

            for (int i=0;i<5;i++){
                System.out.println(accno[i] + "\t" + name[i] + "\t" + bal[i]);
                // display own exception if balance < 1000
                if (bal[i] < 1000)
                {
                    MyException me = new MyException("Balance is less than 1000");
                    throw me;
                }
            }

        }catch (MyException exception){
            exception.printStackTrace();
        }
    }
}