/**
 * Scope of a variable is the part of the program where the variable is accessible. Like C/C++, in Java, all identifiers are lexically (or statically) scoped, i.e.scope of a variable can determined at compile time and independent of function call stack.
 * Java programs are organized in the form of classes. Every class is part of some package. Java scope rules can be covered under following categories.
 *
 * Modifier      Package  Subclass  World
 * public          Yes      Yes     Yes
 *
 * protected       Yes      Yes     No
 *
 * Default (no
 * modifier)       Yes       No     No
 *
 * private         No        No     No
 *
 */

public class ScopeOfVariables {

    static int x = 11;
    private int y = 33;

    public static void main(String[] args) {
        ScopeOfVariables scopeOfVariables = new ScopeOfVariables();
        scopeOfVariables.method1(10);
        scopeOfVariables.method2();
    }
    /**
     * Local Variables (Method Level Scope)
     * Variables declared inside a method have method level scope and can’t be accessed outside the method.
     */

    public void method1(int x){
        ScopeOfVariables test = new ScopeOfVariables();
        this.x = 22;
        y = 44;
        System.out.println("This x => "+ScopeOfVariables.x);
        System.out.println("Test x =>"+test.x);
        System.out.println("Test y =>"+test.y);
        System.out.println("This y =>"+y);

    }

    /**
     * Loop Variables (Block Scope)
     * A variable declared inside pair of brackets “{” and “}” in a method has scope within the brackets only.
     */

    public void method2(){
        for (int i = 0; i < 4; i++)
        {
            System.out.println(i);
        }
        // Will produce error
//        System.out.println(i);
    }

}
